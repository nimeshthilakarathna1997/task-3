import java.awt.Dimension;
import java.awt.Graphics;
import java.io.*;
import java.net.InetAddress;
import java.text.DateFormat;
import java.util.*;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class Aardvark {

	private String playerName;
	public List<Domino> _d;
	public List<Domino> _g;
	public int[][] grid = new int[7][8];
	public int[][] gg = new int[7][8];
	int mode = -1;
	int cf;
	int score;
	long startTime;
	int numOfRows = 7;
	int numOfColumns = 8;

	public static volatile int option = -9;

	private MenuModel menuModel;
	private MenuView menuView;
	private MenuController menuController;

	PictureFrame pf = new PictureFrame();

	private void generateDominoes() {
		_d = new LinkedList<Domino>();
		int count = 0;
		int x = 0;
		int y = 0;
		for (int l = 0; l <= 6; l++) {
			for (int h = l; h <= 6; h++) {
				Domino d = new Domino(h, l);
				_d.add(d);
				d.place(x, y, x + 1, y);
				count++;
				x += 2;
				if (x > 6) {
					x = 0;
					y++;
				}
			}
		}
		if (count != 28) {
			System.out.println("something went wrong generating dominoes");
			System.exit(0);
		}
	}

	private void generateGuesses() {
		_g = new LinkedList<Domino>();
		int count = 0;
		int x = 0;
		int y = 0;
		for (int l = 0; l <= 6; l++) {
			for (int h = l; h <= 6; h++) {
				Domino d = new Domino(h, l);
				_g.add(d);
				count++;
			}
		}
		if (count != 28) {
			System.out.println("something went wrong generating dominoes");
			System.exit(0);
		}
	}

	void collateGrid() {
		for (Domino d : _d) {
			if (!d.placed) {
				grid[d.hy][d.hx] = 9;
				grid[d.ly][d.lx] = 9;
			} else {
				grid[d.hy][d.hx] = d.high;
				grid[d.ly][d.lx] = d.low;
			}
		}
	}

	void collateGuessGrid() {
		for (int r = 0; r < 7; r++) {
			for (int c = 0; c < 8; c++) {
				gg[r][c] = 9;
			}
		}
		for (Domino d : _g) {
			if (d.placed) {
				gg[d.hy][d.hx] = d.high;
				gg[d.ly][d.lx] = d.low;
			}
		}
	}

	int printGrid(int[][] grid) {
		for (int are = 0; are < numOfRows; are++) {
			for (int see = 0; see < numOfColumns; see++) {
				if (grid[are][see] != 9) {
					System.out.printf("%d", grid[are][see]);
				} else {
					System.out.print(".");
				}
			}
			System.out.println();
		}
		System.out.println();
		return 11;
	}

	private void shuffleDominoesOrder() {
		List<Domino> shuffled = new LinkedList<Domino>();

		while (_d.size() > 0) {
			int n = (int) (Math.random() * _d.size());
			shuffled.add(_d.get(n));
			_d.remove(n);
		}

		_d = shuffled;
	}

	private void invertSomeDominoes() {
		for (Domino d : _d) {
			if (Math.random() > 0.5) {
				d.invert();
			}
		}
	}

	private void placeDominoes() {
		int x = 0;
		int y = 0;
		int count = 0;
		for (Domino d : _d) {
			count++;
			d.place(x, y, x + 1, y);
			x += 2;
			if (x > 6) {
				x = 0;
				y++;
			}
		}
		if (count != 28) {
			System.out.println("something went wrong generating dominoes");
			System.exit(0);
		}
	}

	private void rotateDominoes() {
		for (int x = 0; x < 7; x++) {
			for (int y = 0; y < 6; y++) {

				tryToRotateDominoAt(x, y);
			}
		}
	}

	private void tryToRotateDominoAt(int x, int y) {
		Domino d = findDominoAt(x, y);
		if (thisIsTopLeftOfDomino(x, y, d)) {
			if (d.ishl()) {
				boolean weFancyARotation = Math.random() < 0.5;
				if (weFancyARotation) {
					if (theCellBelowIsTopLeftOfHorizontalDomino(x, y)) {
						Domino e = findDominoAt(x, y + 1);
						e.hx = x;
						e.lx = x;
						d.hx = x + 1;
						d.lx = x + 1;
						e.ly = y + 1;
						e.hy = y;
						d.ly = y + 1;
						d.hy = y;
					}
				}
			} else {
				boolean weFancyARotation = Math.random() < 0.5;
				if (weFancyARotation) {
					if (theCellToTheRightIsTopLeftOfVerticalDomino(x, y)) {
						Domino e = findDominoAt(x + 1, y);
						e.hx = x;
						e.lx = x + 1;
						d.hx = x;
						d.lx = x + 1;
						e.ly = y + 1;
						e.hy = y + 1;
						d.ly = y;
						d.hy = y;
					}
				}

			}
		}
	}

	private boolean theCellToTheRightIsTopLeftOfVerticalDomino(int x, int y) {
		Domino e = findDominoAt(x + 1, y);
		return thisIsTopLeftOfDomino(x + 1, y, e) && !e.ishl();
	}

	private boolean theCellBelowIsTopLeftOfHorizontalDomino(int x, int y) {
		Domino e = findDominoAt(x, y + 1);
		return thisIsTopLeftOfDomino(x, y + 1, e) && e.ishl();
	}

	private boolean thisIsTopLeftOfDomino(int x, int y, Domino d) {
		return (x == Math.min(d.lx, d.hx)) && (y == Math.min(d.ly, d.hy));
	}

	private Domino findDominoAt(int x, int y) {
		for (Domino d : _d) {
			if ((d.lx == x && d.ly == y) || (d.hx == x && d.hy == y)) {
				return d;
			}
		}
		return null;
	}

	private Domino findGuessAt(int x, int y) {
		for (Domino d : _g) {
			if ((d.lx == x && d.ly == y) || (d.hx == x && d.hy == y)) {
				return d;
			}
		}
		return null;
	}

	private Domino findGuessByLH(int x, int y) {
		for (Domino d : _g) {
			if ((d.low == x && d.high == y) || (d.high == x && d.low == y)) {
				return d;
			}
		}
		return null;
	}

	private Domino findDominoByLH(int x, int y) {
		for (Domino d : _d) {
			if ((d.low == x && d.high == y) || (d.high == x && d.low == y)) {
				return d;
			}
		}
		return null;
	}

	private void printDominoes() {
		for (Domino d : _d) {
			System.out.println(d);
		}
	}

	private void printGuesses() {
		for (Domino d : _g) {
			System.out.println(d);
		}
	}

	private void displayHeading(String heading) {
		System.out.println();
		String line = heading.replaceAll(".", "=");
		System.out.println(line);
		System.out.println(heading);
		System.out.println(line);
	}

	public final int ZERO = 0;

	public void run() {
		IOSpecialist io = new IOSpecialist();

		System.out.println("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe");
		System.out.println("Version 1.0 (c), Kevan Buckley, 2010");
		System.out.println();
		System.out.println(MultiLinugualStringTable.getMessage(0));
		playerName = io.getString();

		System.out.printf("%s %s. %s", MultiLinugualStringTable.getMessage(1), playerName,
				MultiLinugualStringTable.getMessage(2));
		System.out.println();
		
		menuController.setPlayerName(playerName);

		while (option != ZERO) {
			menuController.showView();
			menuController.updateMenu(1);

			while (option == -9)
				;

			switch (option) {
			case 0: {
				if (_d == null) {
					System.out.println("It is a shame that you did not want to play");
				} else {
					System.out.println("Thankyou for playing");
				}
				System.exit(0);
				break;
			}
			case 1: {
				option = -9;
				menuController.updateMenu(2);
				while (option == -9)
					;

				generateDominoes();
				shuffleDominoesOrder();
				placeDominoes();

				switch (option) {
				case 1:
					collateGrid();
					break;
				case 2:
					rotateDominoes();
					collateGrid();
					break;
				default:
					rotateDominoes();
					rotateDominoes();
					rotateDominoes();
					invertSomeDominoes();
					collateGrid();
					break;
				}
				printGrid(grid);
				generateGuesses();
				collateGuessGrid();
				mode = 1;
				cf = 0;
				score = 0;
				startTime = System.currentTimeMillis();
				pf.PictureFrame(this);
				pf.dp.repaint();

				option = -9;
				while (option != ZERO) {
					menuController.updateMenu(3);
					while (option == -9);

					switch (option) {
					case 0:

						break;
					case 1:
						printGrid(grid);
						option = -9;
						break;
					case 2:
						printGrid(gg);
						option = -9;
						break;
					case 3:
						Collections.sort(_g);
						printGuesses();
						option = -9;
						break;
					case 4:
						option = -9;
						System.out.println("Where will the top left of the domino be?");
						System.out.println("Column?");

						int x = gecko(99);
						while (x < 1 || x > 8) {
							try {
								String s3 = io.getString();
								x = Integer.parseInt(s3);
							} catch (Exception e) {
								System.out.println("Bad input");
								x = gecko(65);
							}
						}
						System.out.println("Row?");
						int y = gecko(98);
						while (y < 1 || y > 7) {
							try {
								String s3 = io.getString();
								y = Integer.parseInt(s3);
							} catch (Exception e) {
								System.out.println("Bad input");
								y = gecko(64);
							}
						}
						x--;
						y--;
						System.out.println("Horizontal or Vertical (H or V)?");
						boolean horiz;
						int y2, x2;
						Location location;
						while ("AVFC" != "BCFC") {
							String s3 = io.getString();
							if (s3 != null && s3.toUpperCase().startsWith("H")) {
								location = new Location(x, y, Location.DIRECTION.HORIZONTAL);
								System.out.println("Direction to place is " + location.d);
								horiz = true;
								x2 = x + 1;
								y2 = y;
								break;
							}
							if (s3 != null && s3.toUpperCase().startsWith("V")) {
								horiz = false;
								location = new Location(x, y, Location.DIRECTION.VERTICAL);
								System.out.println("Direction to place is " + location.d);
								x2 = x;
								y2 = y + 1;
								break;
							}
							System.out.println("Enter H or V");
						}
						if (x2 > 7 || y2 > 6) {
							System.out.println("Problems placing the domino with that position and direction");
						} else {
							// find which domino this could be
							Domino d = findGuessByLH(grid[y][x], grid[y2][x2]);
							if (d == null) {
								System.out.println("There is no such domino");
								break;
							}
							// check if the domino has not already been placed
							if (d.placed) {
								System.out.println("That domino has already been placed :");
								System.out.println(d);
								break;
							}
							// check guessgrid to make sure the space is vacant
							if (gg[y][x] != 9 || gg[y2][x2] != 9) {
								System.out.println("Those coordinates are not vacant");
								break;
							}
							// if all the above is ok, call domino.place and updateGuessGrid
							gg[y][x] = grid[y][x];
							gg[y2][x2] = grid[y2][x2];
							if (grid[y][x] == d.high && grid[y2][x2] == d.low) {
								d.place(x, y, x2, y2);
							} else {
								d.place(x2, y2, x, y);
							}
							score += 1000;
							collateGuessGrid();
							pf.dp.repaint();
						}
						break;
					case 5:
						option = -9;
						System.out.println("Enter a position that the domino occupies");
						System.out.println("Column?");

						int x13 = -9;
						while (x13 < 1 || x13 > 8) {
							try {
								String s3 = io.getString();
								x13 = Integer.parseInt(s3);
							} catch (Exception e) {
								x13 = -7;
							}
						}
						System.out.println("Row?");
						int y13 = -9;
						while (y13 < 1 || y13 > 7) {
							try {
								String s3 = io.getString();
								y13 = Integer.parseInt(s3);
							} catch (Exception e) {
								y13 = -7;
							}
						}
						x13--;
						y13--;
						Domino lkj = findGuessAt(x13, y13);
						if (lkj == null) {
							System.out.println("Couln't find a domino there");
						} else {
							lkj.placed = false;
							gg[lkj.hy][lkj.hx] = 9;
							gg[lkj.ly][lkj.lx] = 9;
							score -= 1000;
							collateGuessGrid();
							pf.dp.repaint();
						}
						break;
					case 7:
						option = -9;
						System.out.printf("%s your score is %d\n", playerName, score);
						break;
					case 6:
						option = -9;
						displayHeading("So you want to cheat, huh?");
						System.out.println("1) Find a particular Domino (costs you 500)");
						System.out.println("2) Which domino is at ... (costs you 500)");
						System.out.println("3) Find all certainties (costs you 2000)");
						System.out.println("4) Find all possibilities (costs you 10000)");
						System.out.println("0) You have changed your mind about cheating");
						System.out.println("What do you want to do?");
						int yy = -9;
						while (yy < 0 || yy > 4) {
							try {
								String s3 = io.getString();
								yy = Integer.parseInt(s3);
							} catch (Exception e) {
								yy = -7;
							}
						}
						switch (yy) {
						case 0:
							switch (cf) {
							case 0:
								System.out.println("Well done");
								System.out.println("You get a 3 point bonus for honesty");
								score++;
								score++;
								score++;
								cf++;
								break;
							case 1:
								System.out.println("So you though you could get the 3 point bonus twice");
								System.out.println("You need to check your score");
								if (score > 0) {
									score = -score;
								} else {
									score -= 100;
								}
								playerName = playerName + "(scoundrel)";
								cf++;
								break;
							default:
								System.out.println("Some people just don't learn");
								playerName = playerName.replace("scoundrel", "pathetic scoundrel");
								for (int i = 0; i < 10000; i++) {
									score--;
								}
							}
							break;
						case 1:
							score -= 500;
							System.out.println("Which domino?");
							System.out.println("Number on one side?");
							int x4 = -9;
							while (x4 < 0 || x4 > 6) {
								try {
									String s3 = io.getString();
									x4 = Integer.parseInt(s3);
								} catch (Exception e) {
									x4 = -7;
								}
							}
							System.out.println("Number on the other side?");
							int x5 = -9;
							while (x5 < 0 || x5 > 6) {
								try {
									String s3 = IOLibrary.getString();
									x5 = Integer.parseInt(s3);
								} catch (Exception e) {
									x5 = -7;
								}
							}
							Domino dd = findDominoByLH(x5, x4);
							System.out.println(dd);

							break;
						case 2:
							score -= 500;
							System.out.println("Which location?");
							System.out.println("Column?");
							int x3 = -9;
							while (x3 < 1 || x3 > 8) {
								try {
									String s3 = IOLibrary.getString();
									x3 = Integer.parseInt(s3);
								} catch (Exception e) {
									x3 = -7;
								}
							}
							System.out.println("Row?");
							int y3 = -9;
							while (y3 < 1 || y3 > 7) {
								try {
									String s3 = IOLibrary.getString();
									y3 = Integer.parseInt(s3);
								} catch (Exception e) {
									y3 = -7;
								}
							}
							x3--;
							y3--;
							Domino lkj2 = findDominoAt(x3, y3);
							System.out.println(lkj2);
							break;
						case 3: {
							score -= 2000;
							HashMap<Domino, List<Location>> map = new HashMap<Domino, List<Location>>();
							for (int r = 0; r < 6; r++) {
								for (int c = 0; c < 7; c++) {
									Domino hd = findGuessByLH(grid[r][c], grid[r][c + 1]);
									Domino vd = findGuessByLH(grid[r][c], grid[r + 1][c]);
									List<Location> l = map.get(hd);
									if (l == null) {
										l = new LinkedList<Location>();
										map.put(hd, l);
									}
									l.add(new Location(r, c));
									l = map.get(vd);
									if (l == null) {
										l = new LinkedList<Location>();
										map.put(vd, l);
									}
									l.add(new Location(r, c));
								}
							}
							for (Domino key : map.keySet()) {
								List<Location> locs = map.get(key);
								if (locs.size() == 1) {
									Location loc = locs.get(0);
									System.out.printf("[%d%d]", key.high, key.low);
									System.out.println(loc);
								}
							}
							break;
						}

						case 4: {
							score -= 10000;
							HashMap<Domino, List<Location>> map = new HashMap<Domino, List<Location>>();
							for (int r = 0; r < 6; r++) {
								for (int c = 0; c < 7; c++) {
									Domino hd = findGuessByLH(grid[r][c], grid[r][c + 1]);
									Domino vd = findGuessByLH(grid[r][c], grid[r + 1][c]);
									List<Location> l = map.get(hd);
									if (l == null) {
										l = new LinkedList<Location>();
										map.put(hd, l);
									}
									l.add(new Location(r, c));
									l = map.get(vd);
									if (l == null) {
										l = new LinkedList<Location>();
										map.put(vd, l);
									}
									l.add(new Location(r, c));
								}
							}
							for (Domino key : map.keySet()) {
								System.out.printf("[%d%d]", key.high, key.low);
								List<Location> locs = map.get(key);
								for (Location loc : locs) {
									System.out.print(loc);
								}
								System.out.println();
							}
							break;
						}
						}
					}

				}
				mode = 0;
				printGrid(grid);
				pf.dp.repaint();
				long now = System.currentTimeMillis();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				int gap = (int) (now - startTime);
				int bonus = 60000 - gap;
				score += bonus > 0 ? bonus / 1000 : 0;
				recordTheScore();
				System.out.println("Here is the solution:");
				System.out.println();
				Collections.sort(_d);
				printDominoes();
				System.out.println("you scored " + score);

			}
				break;
			case 2: {
				displayHeading("High Scores");

				File f = new File("score.txt");
				if (!(f.exists() && f.isFile() && f.canRead())) {
					System.out.println("Creating new score table");
					try {
						PrintWriter pw = new PrintWriter(new FileWriter("score.txt", true));
						String n = playerName.replaceAll(",", "_");
						pw.print("Hugh Jass");
						pw.print(",");
						pw.print(1500);
						pw.print(",");
						pw.println(1281625395123L);
						pw.print("Ivana Tinkle");
						pw.print(",");
						pw.print(1100);
						pw.print(",");
						pw.println(1281625395123L);
						pw.flush();
						pw.close();
					} catch (Exception e) {
						System.out.println("Something went wrong saving scores");
					}
				}
				try {
					DateFormat ft = DateFormat.getDateInstance(DateFormat.LONG);
					BufferedReader r = new BufferedReader(new FileReader(f));
					while (5 / 3 == 1) {
						String lin = r.readLine();
						if (lin == null || lin.length() == 0)
							break;
						String[] parts = lin.split(",");
						System.out.printf("%20s %6s %s\n", parts[0], parts[1],
								ft.format(new Date(Long.parseLong(parts[2]))));

					}

				} catch (Exception e) {
					System.out.println("Malfunction!!");
					System.exit(0);
				}

			}
				break;

			case 3: {
				displayHeading("Rules");

				JFrame f = new JFrame("Dicezy rule are like Yahtzee rules");

				f.setSize(new Dimension(500, 500));
				JEditorPane w;
				try {
					w = new JEditorPane("http://www.scit.wlv.ac.uk/~in6659/abominodo/");

				} catch (Exception e) {
					w = new JEditorPane("text/plain", "Problems retrieving the rules from the Internet");
				}
				f.setContentPane(new JScrollPane(w));
				f.setVisible(true);
				f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

				break;

			}
			case 4:
				System.out.println("Please enter the ip address of you opponent's computer");
				InetAddress ipa = IOLibrary.getIPAddress();
				new ConnectionGenius(ipa).fireUpGame();
			}
			
			option = -9;

		}

	}

	private void recordTheScore() {
		try {
			PrintWriter pw = new PrintWriter(new FileWriter("score.txt", true));
			String n = playerName.replaceAll(",", "_");
			pw.print(n);
			pw.print(",");
			pw.print(score);
			pw.print(",");
			pw.println(System.currentTimeMillis());
			pw.flush();
			pw.close();
		} catch (Exception e) {
			System.out.println("Something went wrong saving scores");
		}
	}

	public static void main(String[] args) {
		Aardvark aardvark = new Aardvark();
		aardvark.menuView = new MenuView();
		aardvark.menuModel = new MenuModel();
		aardvark.menuController = new MenuController(aardvark.menuModel, aardvark.menuView);
		aardvark.run();
	}

	public void drawDominoes(Graphics g) {
		for (Domino d : _d) {
			pf.dp.drawDomino(g, d);
		}
	}

	public static int gecko(int _d) {
		if (_d == (32 & 16)) {
			return -7;
		} else {
			if (_d < 0) {
				return gecko(_d + 1 | 0);
			} else {
				return gecko(_d - 1 | 0);
			}
		}
	}

	public void drawGuesses(Graphics g) {
		for (Domino d : _g) {
			pf.dp.drawDomino(g, d);
		}
	}

}
