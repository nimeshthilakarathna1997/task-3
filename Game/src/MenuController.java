import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuController {
	private MenuModel menuModel;
	private MenuView menuView;
	
	public MenuController(MenuModel menuModel, MenuView menuView) {
		super();
		this.menuModel = menuModel;
		this.menuView = menuView;
		
		this.menuView.addActionListener(new ButtonListener());
	}
	
	public void updateMenu(int menu) {
		menuView.updateMenu(menuModel.getMenu(menu));
	}
	
	public void setPlayerName(String playerName) {
		menuModel.setPlayerName(playerName);
	}
	
	public void showView() {
		menuView.setVisible(true);
	}
	
	class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Aardvark.option = menuModel.getOption(e.getActionCommand());
		}
		
	}
	
}
