import java.util.ArrayList;
import java.util.List;

public class MenuModel {
	private List<String> mainMenu = new ArrayList<String>();
	private List<String> playMenu = new ArrayList<String>();
	private List<String> difficultyMenu = new ArrayList<String>();

	private String playerName;

	public MenuModel() {
		mainMenu.add("Main Menu");
		mainMenu.add("");
		mainMenu.add("1) Play");
		mainMenu.add("2) View high scores");
		mainMenu.add("3) View rules");
		mainMenu.add("0) Quit");

		difficultyMenu.add("Select difficulty");
		difficultyMenu.add("");
		difficultyMenu.add("1) Simples");
		difficultyMenu.add("2) Not-so-simples");
		difficultyMenu.add("3) Super-duper-shuffled");

		playMenu.add("Play menu");
		playMenu.add("");
		playMenu.add("1) Print the grid");
		playMenu.add("2) Print the box");
		playMenu.add("3) Print the dominos");
		playMenu.add("4) Place a domino");
		playMenu.add("5) Unplace a domino");
		playMenu.add("6) Get some assistance");
		playMenu.add("7) Check your score");
		playMenu.add("0) Given up");
	}

	public int getOption(String name) {
		return Character.getNumericValue(name.charAt(0));
	}

	public List<String> getMenu(int menuNumber) {

		if (menuNumber == 1) {
			return mainMenu;
		} else if (menuNumber == 2) {
			return difficultyMenu;
		} else {
			return playMenu;
		}
	}

	public void setPlayerName(String name) {
		this.playerName = name;
		playMenu.set(1,"What do you want to do " + playerName + "?");
	}
}
