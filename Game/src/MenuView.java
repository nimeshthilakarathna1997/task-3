import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class MenuView extends JFrame {

	private JPanel contentPane, buttonPanel;
	private JLabel menuName;
	private JLabel menuPrompt;
	private JButton button;
	private ActionListener actionListener;

	public MenuView() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 725, 395);
		setTitle("Menu");
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel header = new JPanel();
		header.setBackground(new Color(116, 116, 116));
		header.setPreferredSize(new Dimension(100, 120));
		header.setLayout(null);
		contentPane.add(header, BorderLayout.NORTH);

		menuName = new JLabel();
		menuName.setText("Hello");
		menuName.setFont(new Font("Tahoma", Font.PLAIN, 35));
		menuName.setForeground(Color.white);
		menuName.setHorizontalAlignment(SwingConstants.CENTER);
		header.add(menuName);
		menuName.setBounds(0, 0, 715, 74);

		menuPrompt = new JLabel("level 2");
		menuPrompt.setForeground(Color.WHITE);
		menuPrompt.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 14));
		menuPrompt.setBounds(10, 80, 380, 13);
		header.add(menuPrompt);

		buttonPanel = new JPanel();
		buttonPanel.setBackground(new Color(120, 120, 120));
		buttonPanel.setBorder(new EmptyBorder(9, 5, 0, 5));
		buttonPanel.setLayout(new GridLayout(3, 3, 15, 15));
		contentPane.add(buttonPanel, BorderLayout.CENTER);
	}

	public void updateMenu(List<String> menu) {
		buttonPanel.removeAll();
		menuName.setText(menu.get(0));
		menuPrompt.setText(menu.get(1));
		for (int i = 2; i < menu.size(); i++) {
			button = new JButton(menu.get(i));
			button.setFont(new Font("Tahoma", Font.BOLD, 15));
			button.setBackground(new Color(243, 243, 243));
			button.addActionListener(actionListener);
			buttonPanel.add(button);
		}
		buttonPanel.revalidate();
		buttonPanel.repaint();
	}
	
	public void addActionListener(ActionListener actionListener) {
		this.actionListener = actionListener;
	}
}
