
public class Location extends SpacePlace {
  public int y;
  public int x;
  public DIRECTION d;
  
  public enum DIRECTION {VERTICAL, HORIZONTAL};
  
  public Location(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public Location(int x, int y, DIRECTION d) {    
    this(x,y);
    this.d=d;
  }
  
  public String toString() {
    if(d==null){
      return "(" + (y+1) + "," + (x+1) + ")";
    } else {
      return "(" + (y+1) + "," + (x+1) + "," + d + ")";
    }
  }
}
